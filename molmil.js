const fs = require('fs');
const os = require('os');
const yaml = require('js-yaml');
const http = require('http');
const util = require('util');
const child_process = require('child_process');
const querystring = require('querystring');
const fsp = fs.promises;
const mimeTypes = require("mime-types");
const fetch = require("node-fetch");

var PORT = 5498;

// === general ===

const isWsl = function() {
	if (process.platform !== 'linux') return false;

	if (os.release().toLowerCase().includes('microsoft')) return true;

	try {
		if (fs.readFileSync('/proc/version', 'utf8').toLowerCase().includes('microsoft')) return true;
	} catch (_) {return false;}
}();

function wslPath(path) {
  if (! isWsl) return path;
  return path.replace(/\/mnt\/([a-z])\//, "$1:\\").replace(/\//g, "\\");
}

function Deferred() {
  var self = this;
  this.promise = new Promise(function(resolve, reject) {
    self.reject = reject
    self.resolve = resolve
  })
}

// === video functions ===

var ffmpegInstance = null;

async function initVideo(path, width, height, framerate) {
  var args = [ '-r', framerate || '10',
               '-f','image2pipe',
               '-vcodec', 'png',
               '-i', '-', 
               '-s', width+"x"+height,
               '-pix_fmt', 'yuv420p',
               '-an',
               '-vcodec', 'libx264',
               '-r', framerate || '10',
               path
             ];

  ffmpegInstance = child_process.spawn(CONFIG.ffmpeg, args);
  
  ffmpegInstance.stderr.on('data', function(data) {
    console.error(data.toString());
  });
}

function addFrame(framebuffer) {
  var data = Buffer.from(framebuffer.replace(/^data:image\/\w+;base64,/, ""), "base64");
  ffmpegInstance.stdin.write(data);
}

async function finalizeVideo() {
  var waiter = new Deferred();
  ffmpegInstance.on('exit', function() {waiter.resolve();});
  ffmpegInstance.stdin.end();
  ffmpegInstance = null;
  await waiter.promise;
}

// === app based ===

async function setupPage(page, cmd) {
  page.on('console', msg => console.log(msg.text()));
  page.on('pageerror', msg => console.error(msg.toString()));
  var todo = [], tmp;
  page.exposeFunction("local_file_saver", function(path, data, type) {
    if (type == "base64-bin") { // get rid of mime junk
      data = Buffer.from(data.substr(data.indexOf(";base64,")+8), "base64");
      type = "buffer"
    }
    else if (type == "utf8") {
      data = Buffer.from(data, "utf8");
      type = "buffer";
    }
    if (type == "buffer") fs.writeFileSync(path, data);
  });
  
  page.exposeFunction("local_exit_function", async function() {
    await page.close();
  });
  
  if (CONFIG.ffmpeg) {
    page.exposeFunction("initVideo", initVideo);
    page.exposeFunction("addFrame", addFrame);
    page.exposeFunction("finalizeVideo", finalizeVideo);
  }

  page.on("close", async function() {
    var old = await page.browserObject.pages();
    if (old.length == 0) process.exit();
  });

  page.on("load", async function() {
    await page.addScriptTag({path: molmildir+"index.js"});
    page.evaluate(function(cwd, cmd) {init(cwd, cmd);}, `http://localhost:${PORT}/fetch/${CWD}/`, cmd);
  });  
}

async function runApp(cmd) {
  var browser, page;
  try {
    var endpoint = await fsp.readFile(__dirname+"/endpoint.dat");
    browser = await puppeteer.connect({browserWSEndpoint: endpoint, defaultViewport: null});
    page = await browser.newPage();
    page.browserObject = browser;
    await setupPage(page, cmd);
    page.goto(molmilURL);
  }
  catch (e) {
    browser = await puppeteer.launch(options);
    const wsEndpoint = browser.wsEndpoint();
    await fsp.writeFile(__dirname+"/endpoint.dat", wsEndpoint);
    page = (await browser.pages())[0];
    page.browserObject = browser;
    await setupPage(page, cmd);
    
    ['exit', 'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'].forEach(function (sig) {process.on(sig, async function () {
      try {await browser.close();}
      catch (e) {}
    });});
  }
};

async function runBrowser() {
  var browser = await puppeteer.launch(options);

  var old = await browser.pages();
  for (var i=0; i<old.length; i++) old[i].close();
  for (var i=0; i<molmil_cmds.length; i++) {
    const page = await browser.newPage();
    page.browserObject = browser;
    await setupPage(page, molmil_cmds[i]);
    page.goto(molmilURL);
  }
  
  ['exit', 'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'].forEach(function (sig) {process.on(sig, async function () {
    try {await browser.close();}
    catch (e) {}
  });});
};

async function startMolmil() {
  if (appMode) runApp(molmil_cmds[0]);
  else runBrowser();
}

// === web server ===

async function res_output(headers, out, options) {
  headers = headers || {}; out = out || ""; options = options || {};
  if (out && typeof out != "string") out = JSON.stringify(out);
  if (options.file) {
    const stat = await fsp.stat(options.file);
    if ("if-modified-since" in this.reqObj.headers && parseInt(stat.mtime.getTime()/1000) == parseInt(Date.parse(this.reqObj.headers["if-modified-since"])/1000)) {this.writeHead(304); return this.end();}
    
    let fname = options.file;
    if (options.gunzip && fname.endsWith(".gz")) {
      headers["Content-Encoding"] = "gzip";
      fname = fname.slice(0,-3);
    }
    else if (! options.notAttachment) headers["Content-Disposition"] = "attachment; filename="+options.file.split("/").slice(-1)[0];
    headers["Content-Type"] = mimeTypes.lookup(fname) || "application/octet-stream";
    headers["Content-Length"] = stat.size;
    headers["Last-Modified"] = stat.mtime.toUTCString(); // deal with If-Not-Modified-Since header....
  }
  this.writeHead(options.status_code || 200, headers);
  if (options.ondone) this.on("close", options.ondone);
  if (options.file) fs.createReadStream(options.file).pipe(this);
  else {
    if (out) this.write(out);
    this.end();
  }
}

async function fileAccessHandler(req, res) {
  var query = querystring.parse(req.url.split("?")[1]);
  query.has = function(e) {return Object.prototype.hasOwnProperty.call(this, e);};
  query.get = function(key, defaultVal) {return this.has(key) ? this[key] : defaultVal;};

  res.output = res_output;
  res.reqObj = req;

  if (req.url.startsWith("/fetch/")) {
    const loc = decodeURI(req.url).substr(7)
    return res.output({}, null, {file: loc, notAttachment: true});
  }
  else res.output({}, null, {status_code: 404});
}

async function updateMolmil() { // check if this works without an internet connection...
  const molmilCore = "https://molmil.pages.dev/molmil-core.zip";
  const fname = __dirname+"/molmil-core.zip";

  const headers = {};
  try {
    headers["if-none-match"] = await fsp.readFile(__dirname+"/molmil/etag.txt", "utf8");
  }
  catch (e) {} // doesn't exist...
  
  const streamPipeline = util.promisify(require("stream").pipeline);
  let response = await fetch(molmilCore, {headers: headers});
  if (response.status == 304) return;
  if (! response.ok) {
    if (fs.existsSync(__dirname+"/molmil/index.html")) return console.log("Cannot check for new molmil version, using existing one...");
    else {
      console.error("Cannot download molmil code, exiting...");
      process.exit();
    }
  }
  await streamPipeline(response.body, fs.createWriteStream(fname+".tmp"));
  await fsp.rename(fname+".tmp", fname);
  
  const extract = require("extract-zip");
  await extract(fname, { dir: __dirname+"/molmil/" });
  
  await fsp.writeFile(__dirname+"/molmil/etag.txt", response.headers.get("etag"));
}

function initialize() { // don't know if the below is working...
  http.createServer(fileAccessHandler).listen(PORT);

  startMolmil();
};

// === END ===

var username = isWsl ? child_process.execSync('cmd.exe /c "echo %USERNAME%"').toString().trim() : "";

try {var CONFIG = yaml.load(fs.readFileSync(__dirname +"/settings.yml", 'utf8'));}
catch (e) {
  var CONFIG = {"mode": null, "chrome-loc": null, "user-data-dir": null, "molmil-url": "molmil/index.html"};
  var paths = ["/mnt/c/Program Files (x86)/Google/Chrome/Application/chrome.exe", "/mnt/c/Program Files/Google/Chrome/Application/chrome.exe", "/mnt/c/Program Files (x86)/Google/Chrome SxS/Application/chrome.exe", "/mnt/c/Users/"+username+"/AppData/Local/Google/Chrome/Application/chrome.exe", "/mnt/c/Program Files (x86)/Chromium/Application/chrome.exe", "/mnt/c/Program Files (x86)/Microsoft/Edge/Application/msedge.exe", "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome", "/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary", "/Applications/Chromium.app/Contents/MacOS/Chromium", "/usr/bin/google-chrome", "/opt/google/chrome", "/snap/bin/chromium", "/usr/bin/chromium-browser"];
  for (var i=0; i<paths.length; i++) {if (fs.existsSync(paths[i])) {CONFIG["chrome-loc"] = paths[i]; CONFIG.mode = "core"; break;}}
  fs.writeFileSync(__dirname +"/settings.yml", yaml.dump(CONFIG));
}

const coreMode = CONFIG.mode == "core";

if (coreMode) var puppeteer = require('puppeteer-core');
else {
  try {var puppeteer = require('puppeteer');}
  catch {
    console.error("Please install puppeteer to run without local chrome or set chrome-loc to the absolute path to chrome/chromium in settings.yml.");
    process.exit();
  }
}

var chromeLoc = CONFIG["chrome-loc"];
var chromeUDD = CONFIG["user-data-dir"];
var molmilURL = CONFIG["molmil-url"] || "molmil/index.html";
if (! molmilURL.startsWith("https://") && ! molmilURL.startsWith("/")) molmilURL = `http://localhost:${PORT}/fetch/${__dirname}/${molmilURL}`

if (coreMode) {
  if (! chromeLoc || ! fs.existsSync(chromeLoc)) {
    console.error("Please set chrome-loc to the absolute path to chrome/chromium in settings.yml.")
    process.exit();
  }
  if (! chromeUDD) {
    CONFIG["user-data-dir"] = chromeUDD = "puppeteer_user_data";
    fs.writeFileSync(__dirname +"/settings.yml", yaml.dump(CONFIG));
  }
  if (! chromeUDD.startsWith("/")) chromeUDD = __dirname + "/" + chromeUDD;
}
if (coreMode && ! fs.existsSync(chromeUDD)) fs.mkdirSync(chromeUDD);

chromeUDD = wslPath(chromeUDD);

//var CWD = wslPath(process.cwd());
const CWD = process.cwd();

if (isWsl && ! fs.existsSync(__dirname+"/molmil.bat")) {
  const batScript = `echo off

set FilePath=%1
set FilePath=%FilePath:\\=/%
echo %~n1%~x1

if "%~1"=="" (
  powershell -windowstyle hidden -command "&{ bash -c 'node ${__dirname}/molmil.js' }"
) else (
  powershell -windowstyle hidden -command "&{ bash -c 'node ${__dirname}/molmil.js load %~n1%~x1' }"
)
`;
  fs.writeFileSync(__dirname +"/molmil.bat", batScript);

  var CWD_wsl = wslPath(__dirname);

  var createShortcutCmd = `cmd.exe /C powershell.exe -ExecutionPolicy Bypass -NoLogo -NonInteractive -NoProfile -Command "\\$ws = New-Object -ComObject WScript.Shell; \\$s = \\$ws.CreateShortcut('%USERPROFILE%\\Desktop\\Molmil.lnk'); \\$s.TargetPath = '${CWD_wsl}\\molmil.bat'; \\$S.IconLocation='${CWD_wsl}\\molmil\\molmil.ico'; \\$s.Save()"`;
  child_process.execSync(createShortcutCmd);
}


if (fs.existsSync(molmilURL)) molmilURL = "file://" + wslPath(molmilURL);

var molmildir = __dirname+"/";
var molmil_cmds = [], tmp = process.argv.splice(2), bfr = [], flags = [];

var flags = [];
for (var i=0; i<tmp.length; i++) {
  if (tmp[i].startsWith("--")) {flags.push(tmp[i]); continue;}
  if (tmp[i].includes(" ") || tmp[i].includes(";")) {
    if (bfr.length) {molmil_cmds.push(bfr.join(" ")); bfr = [];}
    molmil_cmds.push(tmp[i]);
  }
  else bfr.push(tmp[i]);
}
if (bfr.length) molmil_cmds.push(bfr.join(" "));

if (molmil_cmds.length == 0) molmil_cmds.push("");

var options = {
  headless: false, 
  defaultViewport: null,
  ignoreDefaultArgs: ['--enable-automation', '--enable-blink-features=IdleDetection'],
  args: ['--allow-file-access-from-file', '--webgl-msaa-sample-count=8', '--disable-features=site-per-process']
};

if (coreMode) {
  options.executablePath = chromeLoc;
  options.args.push(`--user-data-dir=${chromeUDD}`)
}

var appMode = false;

if (flags.includes("--headless")) options.headless = true;
else appMode = true;

if (flags.includes("--no-app") || molmil_cmds.length > 1) appMode = false;
if (appMode) options.args.push('--app='+molmilURL);

updateMolmil().then(initialize);

