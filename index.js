function init(cwd, cmd) {
  var canvas = document.body.getElementsByTagName("canvas")[0];
  if (! canvas.commandLine || ! canvas.commandLine.eval) return setTimeout(function() {init(cwd, cmd);}, 100);

  molmil.__cwd__ = cwd;
  
  molmil.configBox.customSaveFunction = local_file_saver;
  molmil.configBox.customExitFunction = local_exit_function;
  
  canvas.commandLine.environment.initVideo = window.initVideo;
  canvas.commandLine.environment.addFrame = window.addFrame;
  canvas.commandLine.environment.finalizeVideo = window.finalizeVideo;
  
  var cmds = cmd.split(";");
  for (var i=0; i<cmds.length; i++) canvas.commandLine.environment.console.runCommand(cmds[i]);
}
