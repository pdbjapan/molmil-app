### About

This app enables execution of molmil (https://pdbj.org/molmil2/) as a local application, allowing it to read locally defined files and also execute it in an automated, headless mode.

### Usage

This app requires nodejs (latest LTS from https://nodejs.org/en/download/ or via your OS' package manager). 

Then, obtain molmil-app from the repository:  
`git clone https://gitlab.com/pdbjapan/molmil-app.git`  
`cd molmil-app`

And install the dependencies (via nodejs' package manager):  
`npm install`

Molmil-app can run in two modes; using a locally installed version of chrome/chromium, or using embedded chromium. 

- To setup embedded chromium, execute the following command:  
`npm install puppeteer`

- Alternatively, to use a locally installed version of chrome/chromium, first try and run molmil-app, as it'll try to detect chrome automatically. If that fails, open `settings.yml` using an editor and set `mode` to "core" and `chrome-loc` to the absolute path to the chrome executable.

To start molmil-app, execute the following command:  
`node molmil.js`

Molmil commands can also be added directly as follows:  
`node molmil.js "fetch 1crn; show sticks, sidechain; color cyan, symbol C;"`

Files can also be loaded from the local hard drive and png images can be directly saved:  
`node molmil.js "load pdb1crn.ent; png 1crn.png;"`

Of course, `.mjs` files can also be loaded in a similar manner, where `load` commands defined inside will look for the files starting from the current directory (where the command was executed).

Finally, molmil-app can also be executed headlessly as follows and multiple jobs can be executed:  
`node molmil.js --headless "load pdb1crn.ent; png 1crn.png; quit;" "fetch 2prg; png 2prg.png; quit;"`

This automatically executes the two sets of commands, producing two png files and it automatically exits (the `quit` command terminates the job).

To simplify usage, it is possible to set an alias using bash:  
`alias molmil='node /path/to/molmil.js'`

So that molmil-app can be can be executed as:  
`molmil --headless "load pdb1crn.ent; png 1crn.png; quit;" "fetch 2prg; png 2prg.png; quit;"`
